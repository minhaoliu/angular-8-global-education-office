## Disable access-control-allow-origin in browser first

## Start the sever with Laragon
![server](/uploads/e2ad6b5fbc8a8bddc14ee3768aae4290/server.PNG)
<br />

## Login Page
![login](/uploads/ba0987d42494f65551681cc7484431e5/login.PNG)
<br />

## ngForm Module
![FormSubmission](/uploads/9a98f4a506a865ad42a515594366d652/FormSubmission.PNG)
<br />

## Retrieve Table info through HttpClient Call
![code](/uploads/1f9a366fe58fcf6512978b2936f5a486/code.PNG)
![viewForms](/uploads/98831c7904f364023e3550f2161e7bfa/viewForms.PNG)
<br />

## Edit student info
![edit](/uploads/cbafa438f3fc08cd58ea8b6619487f09/edit.PNG)
<br />

## Payment for event ticket
![payment](/uploads/1bcba3f80c4f845cd983330ce9de502b/payment.PNG)
<br />


# GlobalEducation

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
