import { throwError } from 'rxjs';
import { JitCompiler } from '@angular/compiler';

/**
 * create this class depends on backend doc,  please don't change it if not necessary.
 * Any changes of interface will changes the UI data structure;
 */
export interface AirportInfo {
  name: string;
  email: string;
  nationality: string;
  arrivalAirport: string;
  flightNumber: string;
  arrivingDate: string;
  contactNumber: string;
  message: string;
}

export interface User {
  email: string;
  password: string;
  lastName: string;
  firstName: string;
  status: UserStatus;
  country: string;
  id: string;
  department: string;
}

export interface Mentorship {
  studentName: string;
  studentEmail: string;
  mentorName: string;
  mentorEmail: string;
}

export interface Event {
  name: string;
  place: string;
  dateTime: string;
  details: string;
}

export enum UserStatus {
  Mentor = 0,
  Mentee = 1,
}

export const STATE_LIST = [
  'Alabama',
  'Alaska',
  'Arizona',
  'Arkansas',
  'California',
  'Colorado',
  'Connecticut',
  'Delaware',
  'Florida',
  'Georgia',
  'Hawaii',
  'Idaho',
  'Illinois',
  'Indiana',
  'Iowa',
  'Kansas',
  'Kentucky',
  'Louisiana',
  'Maine',
  'Maryland',
  'Massachusetts',
  'Michigan',
  'Minnesota',
  'Mississippi',
  'Missouri',
  'Montana',
  'Nebraska',
  'Nevada',
  'New Hampshire',
  'New Jersey',
  'New Mexico',
  'New York',
  'North Carolina',
  'North Dakota',
  'Ohio',
  'Oklahoma',
  'Oregon',
  'Pennsylvania',
  'Rhode Island',
  'South Carolina',
  'South Dakota',
  'Tennessee',
  'Texas',
  'Utah',
  'Vermont',
  'Virginia',
  'Washington',
  'West Virginia',
  'Wisconsin',
  'Wyoming',
];


export function handleError(error) {
  let errorMessage = '';
  if (error.error instanceof ErrorEvent) {
    // client-side error
    errorMessage = `Error: ${error.error.message}`;
  } else {
    // server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
}

export const MockUser: User[] = [
  {
    email: 'Jiachen.Ji@mu.edu',
    password: '123',
    lastName: 'ji',
    firstName: 'jiachen',
    status: UserStatus.Mentee,
    country: 'USA',
    id: '1234567',
    department: 'Literature'
  },
  {
    email: 'sqi@mu.edu',
    password: '123',
    lastName: 'qi',
    firstName: 'si',
    status: UserStatus.Mentee,
    country: 'USA',
    id: '1234567',
    department: 'Literature'
  },

  {
    email: 'test@mu.edu',
    password: '321',
    lastName: 'test',
    firstName: 'Test',
    status: UserStatus.Mentor,
    country: 'USA',
    id: '1234567',
    department: 'Literature'
  },
  {
    email: 'Jennifer.Pinsker@mu.edu',
    password: '123',
    lastName: 'Pinsker',
    firstName: 'Jennifer',
    status: UserStatus.Mentee,
    country: 'USA',
    id: '1234567',
    department: 'Literature'
  },
  {
    email: 'John.Boo@mu.edu',
    password: '123',
    firstName: 'Boo',
    lastName: 'John',
    status: UserStatus.Mentor,
    country: 'USA',
    id: '1234567',
    department: 'Literature'
  },
  {
    email: 'Bob.Robson@mu.edu',
    password: '123',
    firstName: 'Bob',
    lastName: 'Robson',
    status: UserStatus.Mentee,
    country: 'USA',
    id: '1234567',
    department: 'Literature'
  },
  {
    email: 'Mick.Robins@mu.edu',
    password: '123',
    firstName: 'Mick',
    lastName: 'Robins',
    status: UserStatus.Mentee,
    country: 'USA',
    id: '1234567',
    department: 'Literature'
  },
  {
    email: 'Michael.Robinson@mu.edu',
    password: '123',
    lastName: 'Robinson',
    firstName: 'Michael',
    status: UserStatus.Mentor,
    country: 'USA',
    id: '1234567',
    department: 'Literature'
  },
];

export const MockAirportList: AirportInfo[] = [
  {
    name: 'Jiachen Ji',
    email: 'Jiachen.Ji@mu.edu',
    nationality: 'China',
    arrivalAirport: 'ewr',
    flightNumber: '123',
    arrivingDate: '2019-12-30T03:03',
    contactNumber: '1231231234',
    message: 'hello',
  },
  {
    name: 'qi si',
    email: 'sqi@mu.edu',
    nationality: 'China',
    arrivalAirport: 'ewr',
    flightNumber: '321',
    arrivingDate: '2019-12-20T03:03',
    contactNumber: '1231231234',
    message: 'hello',
  },
];

export const MENTOR_MATCHES: Mentorship[] = [
  {
    studentName: 'Jennifer Pinsker',
    studentEmail: 'Jennifer.Pinsker@mu.edu',
    mentorName: 'John Boo',
    mentorEmail: 'John.Boo@mu.edu',
  },
  {
    studentName: 'Bob Robson',
    studentEmail: 'Bob.Robson@mu.edu',
    mentorName: 'Michael Robinson',
    mentorEmail: 'Michael.Robinson@mu.edu',
  },
  {
    studentName: 'Mick Robins',
    studentEmail: 'Mick.Robins@mu.edu',
    mentorName: 'Michael Robinson',
    mentorEmail: 'Michael.Robinson@mu.edu',
  },
];

interface Pattern {
  name: RegExp;
  password: RegExp;
  mobnum: RegExp;
  email: RegExp;
  id: RegExp;
}
export const PATTERN: Pattern = {
  name: RegExp(/^[a-zA-Z0-9_-]{2,15}$/),
  password: RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,20}$/),
  mobnum: RegExp(/^((\\+91-?)|0)?[0-9]{10}$/),
  email: RegExp(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/),
  id: RegExp(/^[0-9]+$/),
};

export const EVENT_LIST: Event[] = [
  {
    name: 'Thanksgiving',
    place: 'MU campus',
    dateTime: '2019-11-28T17:30',
    details: 'Holiday celebration, traditional food and drinks will be served.'
  },
  {
    name: 'Christmas Day',
    place: 'MU campus',
    dateTime: '2019-12-25T11:30',
    details: 'Holiday celebration, traditional food and drinks will be served.'
  },
  {
    name: 'Independence Day',
    place: 'Wilson Hall',
    dateTime: '2020-07-04T17:00',
    details: 'Holiday celebration, traditional food and drinks will be served.'
  },
  {
    name: 'divali',
    place: 'Wilson Hall',
    dateTime: '2020-11-01T03:03',
    details: 'Indian holiday celebration, traditional food and drinks will be served.'
  }
];
