import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, zh_CN, en_US } from 'ng-zorro-antd';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgForm} from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';



import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { MainpageComponent } from './container/mainpage/mainpage.component';
import { LoginComponent } from './container/login/login.component';
import { HeaderComponent } from './container/header/header.component';
import { FooterComponent } from './container/footer/footer.component';
import { AirportformComponent } from './container/airportform/airportform.component';
import { MentorApplicationComponent } from './container/mentor-application/mentor-application.component';
import { RegisterComponent } from './container/register/register.component';
import { ViewStudentsComponent } from './container/view-students/view-students.component';
import { CreateEventComponent } from './container/create-event/create-event.component';
import { ViewEventComponent } from './container/view-event/view-event.component';
import { ViewpickupComponent } from './container/viewpickup/viewpickup.component';
import { WeatherWidgetComponent } from './container/weather-widget/weather-widget.component';
import { MobileValidatorDirective } from './directives/mobile-validator.directive';
import { EqualValidatorDirective } from './directives/equal-validator.directive';
import { CookieService } from 'ngx-cookie-service';
import { KanbanComponent } from './container/kanban/kanban.component';
import { DisplayMentorComponent } from './container/display-mentor/display-mentor.component';
import { PaymeComponent } from './container/payme/payme.component';
import { MumapComponent } from './container/mumap/mumap.component';
registerLocaleData(en);


@NgModule({
  declarations: [
    AppComponent,
    MainpageComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    AirportformComponent,
    MentorApplicationComponent,
    RegisterComponent,
    ViewStudentsComponent,
    CreateEventComponent,
    ViewEventComponent,
    ViewpickupComponent,
    WeatherWidgetComponent,
    MobileValidatorDirective,
    EqualValidatorDirective,
    KanbanComponent,
    DisplayMentorComponent,
    PaymeComponent,
    MumapComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    HttpClientJsonpModule,
    FormsModule,
    NgZorroAntdModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
