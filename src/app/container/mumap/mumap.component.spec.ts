import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MumapComponent } from './mumap.component';

describe('MumapComponent', () => {
  let component: MumapComponent;
  let fixture: ComponentFixture<MumapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MumapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MumapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
