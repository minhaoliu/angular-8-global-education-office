import { Component, OnInit } from '@angular/core';
import { } from 'googlemaps';
import { ViewChild} from '@angular/core';


@Component({
  selector: 'app-mumap',
  templateUrl: './mumap.component.html',
  styleUrls: ['./mumap.component.less'],

})
export class MumapComponent implements OnInit {
  @ViewChild('map', {static: true})
  mapElement: any;
  map: google.maps.Map;
  labels = 'MU';
  labelIndex = 2;
  constructor() { }

  ngOnInit(): void {

    const mapProperties = {
      center: new google.maps.LatLng(40.281010, -74.005407),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapProperties);

    // marker
    var bangalore = { lat: 40.281010, lng: -74.005407 };


    // This event listener calls addMarker() when the map is clicked.
    google.maps.event.addListener(this.map, 'click', function(event) {
    this.addMarker(event.latLng, this.map);
    });

    // Add a marker at the center of the map.
    this.addMarker(bangalore, this.map);
  }

  addMarker(location, map) {
    // Add the marker at the clicked location, and add the next-available label
    // from the array of alphabetical characters.
    var marker = new google.maps.Marker({
      position: location,
      label: this.labels[this.labelIndex],
      map: map
    });
  }

}
