import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { CookieService } from 'ngx-cookie-service';
import { Board } from '../models/board.model';
import { Column } from '../models/column.model';


@Component({
  selector: 'app-kanban',
  templateUrl: './kanban.component.html',
  styleUrls: ['./kanban.component.scss']
})
export class KanbanComponent implements OnInit {

  private condition = false;
  board: Board = new Board('Test Board', [
    new Column('Ideas', [
      'Some random idea',
      'This is another random idea',
      'build a kanban boards'
    ]),
    new Column('Research', [
      'Lorem Ipsum',
      'Have a coffee',
      'This was in the \'Research\' column'
    ]),
    new Column('Todo', [
      'Find another Graduate Assistant',
      'Schedule airport pickup for Furkan',
      'Clock out',
      'Clock in'
    ]),
    new Column('Done', [
      'Assign Mentor for Furkan',
      'Fill form i-94',
      'Assign Mentor for Qi',
      'Check e-mail',
      'Assign Mentor for Jiachen'
    ])

  ]);


  constructor(private cookieService: CookieService) { }

  ngOnInit() {
    this.condition = this.cookieService.check('loginCheck');
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }



}
