import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { NgForm, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { mobileValidator, emailValidator } from 'src/app/validator/validators';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-viewpickup',
  templateUrl: './viewpickup.component.html',
  styleUrls: ['./viewpickup.component.less']
})
export class ViewpickupComponent implements OnInit {
  private condition = false;
  private isVisible = false;
  private isVisible2 = false;
  isOkLoading = false;
  private firstname: any;
  private lastname: any;
  private email: any;
  private phonenumber: any;
  private arrivaldate: any;
  private arrivaltime: any;
  private arrivalairport: any;
  private flightnumber: any;
  private nationality: any;
  private message: any;
  formModel: FormGroup;
  list;
  deleteId;
  private headers = new Headers({'Content-Type': 'application/json'})

  constructor(
    private http: HttpClient,
    private jsonp: HttpClientJsonpModule,
    private cookieService: CookieService,
    fb: FormBuilder) {
      this.formModel = fb.group({
        firstname: ['', [Validators.required]],
        lastname: ['', [Validators.required]],
        email: ['', [emailValidator, Validators.required]],
        phonenumber: ['', mobileValidator],
        arrivaldate: ['', [Validators.required]],
        arrivaltime: ['', [Validators.required]],
        arrivalairport: ['', [Validators.required]],
        flightnumber: ['', [Validators.required]],
        nationality: [''],
        message: [''],
      });
    }

   // Request data
   getData() {
    this.http.get('http://globalportal.test/pickups').subscribe((data)=>{
      console.log(data);
      this.list = data;
    });
  }

  delete(id){
    this.http.get('http://globalportal.test/pickup/delete/' + id).subscribe((data)=>{
      console.log(data);
      this.isVisible = true;
    });
  }

  edit(id) {
    this.deleteId = id;
    this.list.forEach( item => {
      if (item.id === id) {
        this.firstname = item.firstname;
        this.lastname = item.lastname;
        this.email = item.email;
        this.phonenumber = item.phonenumber;
        this.arrivaldate = item.arrivaldate;
        this.arrivaltime = item.arrivaltime;
        this.arrivalairport = item.arrivalairport;
        this.flightnumber = item.flightnumber;
        this.nationality = item.nationality;
        this.message = item.message;
      }
    });
    this.isVisible2 = true;
  }

  handleOk(): void {
    this.isVisible = false;
    location.reload();
  }

  handleOk2(f: NgForm, id): void {
    this.http.patch('http://globalportal.test/pickup/edit/' + id,
        {
          firstname: f.controls['firstname'].value,
          lastname: f.controls['lastname'].value,
          email: f.controls['email'].value,
          phonenumber: f.controls['phonenumber'].value,
          arrivaldate: f.controls['arrivaldate'].value,
          arrivaltime: f.controls['arrivaltime'].value,
          arrivalairport: f.controls['arrivalairport'].value,
          flightnumber: f.controls['flightnumber'].value,
          nationality: f.controls['nationality'].value,
          message: f.controls['message'].value,
        })
        .subscribe(
          data => {
            console.log('POST Request is successful', data);
            setTimeout( () => {
              this.isVisible2 = false;
              this.isOkLoading = false;
            }, 3000);
            setTimeout( () => {
              this.isVisible = true;
            }, 4000);
          },
          error => {
            console.log('Error', error);
          });
    this.isOkLoading = true;

  }

  handleCancel(): void {
    this.isVisible = false;
    location.reload();
  }

  handleCancel2(): void {
    console.log('Button cancel clicked!');
    this.isVisible2 = false;
  }

  ngOnInit() {
    this.getData();
    this.condition = this.cookieService.check('loginCheck');
  }

}
