import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpickupComponent } from './viewpickup.component';

describe('ViewpickupComponent', () => {
  let component: ViewpickupComponent;
  let fixture: ComponentFixture<ViewpickupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpickupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpickupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
