import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { equalValidator, mobileValidator, emailValidator } from 'src/app/validator/validators';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-airportform',
  templateUrl: './airportform.component.html',
  styleUrls: ['./airportform.component.less']
})
export class AirportformComponent implements OnInit {
  private condition = false;
  private isVisible = false;
  private isVisibleError = false;
  private isVisibleAll = false;
  list;
  private headers = new Headers({ 'Content-Type': 'application/json' });

  formModel: FormGroup;

  // constructor(fb: FormBuilder) {
  //   this.formModel = fb.group({
  //     username: ['', [Validators.required, Validators.minLength(6)]],
  //     mobile: ['', mobileValidator],
  //     passwordGroup: fb.group({
  //       password: ['', Validators.minLength(6)],
  //       pconfirm: ['']
  //     }, {validator: equalValidator})
  //   });

  //  }

  constructor(private http: HttpClient, private jsonp: HttpClientJsonpModule, fb: FormBuilder, private cookieService: CookieService) {
    this.formModel = fb.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      email: ['', [emailValidator, Validators.required]],
      phonenumber: ['', mobileValidator],
      arrivaldate: ['', [Validators.required]],
      arrivaltime: ['', [Validators.required]],
      arrivalairport: ['', [Validators.required]],
      flightnumber: ['', [Validators.required]],
      nationality: [''],
      message: [''],

    });
  }


  // Post Data
  postData(f: NgForm) {

    if (this.formModel.valid) {
      this.http.post('http://globalportal.test/pickup/new',
        {
          firstname: f.controls['firstname'].value,
          lastname: f.controls['lastname'].value,
          email: f.controls['email'].value,
          phonenumber: f.controls['phonenumber'].value,
          arrivaldate: f.controls['arrivaldate'].value,
          arrivaltime: f.controls['arrivaltime'].value,
          arrivalairport: f.controls['arrivalairport'].value,
          flightnumber: f.controls['flightnumber'].value,
          nationality: f.controls['nationality'].value,
          message: f.controls['message'].value,
        })
        .subscribe(
          data => {
            console.log('POST Request is successful', data);
            this.isVisible = true;
          },
          error => {
            console.log('Error', error);
            this.isVisibleError = true;
          });
    } else {
      this.isVisibleAll = true;
    }
  }

  ngOnInit() {
    this.condition = this.cookieService.check('loginCheck');
  }

  handleOk(): void {
    this.isVisible = false;
    location.reload();
  }

  handleCancel(): void {
    this.isVisible = false;
    location.reload();
  }

  handleOkError(): void {
    this.isVisibleError = false;
  }

  handleCancelError(): void {
    this.isVisibleError = false;
  }

  handleOkAll(): void {
    this.isVisibleAll = false;
  }

  handleCancelAll(): void {
    this.isVisibleAll = false;
  }

  onSubmit() {
    // let isValid: boolean = this.formModel.get("username").valid;
    // console.log("username的校验结果：" + isValid);
    // let errors: any = this.formModel.get("username").errors;
    // console.log("username的错误信息是：" + JSON.stringify(errors));
    if (this.formModel.valid) {
      console.log(this.formModel.value);
    }

  }


}
