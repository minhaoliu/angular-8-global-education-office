import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { emailValidator } from 'src/app/validator/validators';

@Component({
  selector: 'app-view-students',
  templateUrl: './view-students.component.html',
  styleUrls: ['./view-students.component.less']
})
export class ViewStudentsComponent implements OnInit {
  private condition = false;
  private isVisible = false;
  private isVisible2 = false;
  isOkLoading = false;
  private firstname: any;
  private lastname: any;
  private studentid: any;
  private email: any;
  private password: any;
  private country: any;
  private department: any;
  formModel: FormGroup;
  list;
  deleteId;
  private headers = new Headers({'Content-Type': 'application/json'})

  constructor(
    private http: HttpClient,
    private jsonp: HttpClientJsonpModule,
    private cookieService: CookieService,
    fb: FormBuilder) {
      this.formModel = fb.group({
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        studentid: ['', Validators.required],
        email: ['', [emailValidator, Validators.required]],
        password: ['', Validators.required],
        country: [''],
        department: [''],
      });
    }

  // Request data
  getData() {
    this.http.get('http://globalportal.test/users').subscribe((data)=>{
      console.log(data);
      this.list = data;
    });
  }

  delete(id){
    this.http.get('http://globalportal.test/user/delete/' + id).subscribe((data)=>{
      console.log(data);
      this.isVisible = true;
    });
  }

  edit(id) {
    this.deleteId = id;
    this.list.forEach( item => {
      if (item.id === id) {
        this.firstname = item.firstname;
        this.lastname = item.lastname;
        this.studentid = item.studentid;
        this.email = item.email;
        this.password = item.password;
        this.country = item.country;
        this.department = item.department;
      }
    });
    this.isVisible2 = true;
  }

  handleOk(): void {
    this.isVisible = false;
    location.reload();
  }

  handleOk2(f: NgForm, id): void {
    this.http.patch('http://globalportal.test/user/edit/' + id,
        {
          firstname: f.controls['firstname'].value,
          lastname: f.controls['lastname'].value,
          studentid: f.controls['studentid'].value,
          email: f.controls['email'].value,
          password: f.controls['password'].value,
          country: f.controls['country'].value,
          department: f.controls['department'].value,
        })
        .subscribe(
          data => {
            console.log('POST Request is successful');
            setTimeout( () => {
              this.isVisible2 = false;
              this.isOkLoading = false;
            }, 3000);
            setTimeout( () => {
              this.isVisible = true;
            }, 4000);
          },
          error => {
            console.log('Error', error);
          });
    this.isOkLoading = true;
  }

  handleCancel(): void {
    this.isVisible = false;
    location.reload();
  }

  handleCancel2(): void {
    console.log('Button cancel clicked!');
    this.isVisible2 = false;
  }

  ngOnInit() {
    this.getData();
    this.condition = this.cookieService.check('loginCheck');
  }

}
