import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-view-event',
  templateUrl: './view-event.component.html',
  styleUrls: ['./view-event.component.less']
})
export class ViewEventComponent implements OnInit {
  private condition = false;
  private isVisible = false;
  private isVisible2 = false;
  private ticket = false;
  isOkLoading = false;
  private date: any;
  private time: any;
  private eventname: any;
  private eventplace: any;
  private description: any;
  formModel: FormGroup;
  list;
  deleteId;
  private headers = new Headers({'Content-Type': 'application/json'})

  constructor(
    private http: HttpClient,
    private jsonp: HttpClientJsonpModule,
    private cookieService: CookieService,
    fb: FormBuilder) {
      this.formModel = fb.group({
        eventname: ['', [Validators.required]],
        eventdate: ['', [Validators.required]],
        eventtime: ['', [Validators.required]],
        eventplace: ['', [Validators.required]],
        eventdescription: [''],

      });
    }

  // Request data
  getData() {
    this.http.get('http://globalportal.test/events').subscribe((data)=>{
      console.log(data);
      this.list = data;
    });
  }

  delete(id) {
    this.http.get('http://globalportal.test/event/delete/' + id).subscribe((data)=>{
      console.log(data);
      this.isVisible = true;
    });
  }

  edit(id) {
    this.deleteId = id;
    this.list.forEach( item => {
      if (item.id === id) {
        this.date = item.date;
        this.time = item.time;
        this.eventname = item.eventname;
        this.eventplace = item.eventplace;
        this.description = item.description;
      }
    });
    this.isVisible2 = true;
  }

  handleOk(): void {
    this.isVisible = false;
    location.reload();
  }

  handleOk2(f: NgForm, id): void {
    this.http.patch('http://globalportal.test/event/edit/' + id,
        {
          date: f.controls['eventdate'].value,
          time: f.controls['eventtime'].value,
          eventname: f.controls['eventname'].value,
          eventplace: f.controls['eventplace'].value,
          description: f.controls['eventdescription'].value,
        })
        .subscribe(
          data => {
            console.log('POST Request is successful');
            setTimeout( () => {
              this.isVisible2 = false;
              this.isOkLoading = false;
            }, 3000);
            setTimeout( () => {
              this.isVisible = true;
            }, 4000);
          },
          error => {
            console.log('Error', error);
          });
    this.isOkLoading = true;
  }

  handleCancel(): void {
    this.isVisible = false;
    location.reload();
  }

  handleCancel2(): void {
    console.log('Button cancel clicked!');
    this.isVisible2 = false;
  }

  handleOk3(): void {
    this.ticket = false;
  }

  handleCancel3(): void {
    this.ticket = false;
  }

  openticket() {
    this.ticket = true;
  }

  ngOnInit() {
    this.getData();
    this.condition = this.cookieService.check('loginCheck');
  }

}
