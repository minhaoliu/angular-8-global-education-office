import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.less']
})
export class MainpageComponent implements OnInit {
  private condition = false;

  constructor(
    private cookieService: CookieService
  ) { }

  ngOnInit() {
    this.condition = this.cookieService.check('loginCheck');
  }

}


