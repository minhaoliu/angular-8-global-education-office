import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { NgForm, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.less']
})
export class CreateEventComponent implements OnInit {
  private condition = false;
  private isVisible = false;
  private isVisibleError = false;
  private isVisibleAll = false;
  list;
  private headers = new Headers({ 'Content-Type': 'application/json' });

  formModel: FormGroup;

  constructor(private http: HttpClient, private jsonp: HttpClientJsonpModule, fb: FormBuilder, private cookieService: CookieService) {
    this.formModel = fb.group({
      eventname: ['', [Validators.required]],
      eventdate: ['', [Validators.required]],
      eventtime: ['', [Validators.required]],
      eventplace: ['', [Validators.required]],
      eventdescription: [''],

    });
  }

  //Post Data
  postData(f: NgForm) {

    if (this.formModel.valid) {
      this.http.post('http://globalportal.test/event/new',
        {
          date: f.controls['eventdate'].value,
          time: f.controls['eventtime'].value,
          eventname: f.controls['eventname'].value,
          eventplace: f.controls['eventplace'].value,
          description: f.controls['eventdescription'].value,
        })
        .subscribe(
          data => {
            console.log('POST Request is successful', data);
            this.isVisible = true;
          },
          error => {
            console.log('Error', error);
            this.isVisibleError = true;
          });
    } else {
      this.isVisibleAll = true;
    }
  }

  handleOk(): void {
    this.isVisible = false;
    location.reload();
  }

  handleCancel(): void {
    this.isVisible = false;
    location.reload();
  }

  handleOkError(): void {
    this.isVisibleError = false;
  }

  handleCancelError(): void {
    this.isVisibleError = false;
  }

  handleOkAll(): void {
    this.isVisibleAll = false;
  }

  handleCancelAll(): void {
    this.isVisibleAll = false;
  }

  ngOnInit() {
    this.condition = this.cookieService.check('loginCheck');
  }

}
