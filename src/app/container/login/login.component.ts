import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { User} from '../../../utils';
import {Router, ActivatedRoute} from '@angular/router';
import { UserInfoService } from '../../service/user-info.service';
import { AccountService } from '../../service/account.service';
import { error } from '@angular/compiler/src/util';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})


export class LoginComponent implements OnInit {
  passwordVisible = false;
  private cookieValue: string;
  private isVisible = false;

  // profileForm = this.fb.group({
  //   email: ['', [Validators.email, Validators.required]],
  //   password: ['', Validators.required],
  // }) as FormGroup;

  // get email() { return this.profileForm.get('email') as FormControl; }

  // get password() { return this.profileForm.get('password') as FormControl; }

  isInvalid = false;
  userInfo: User | undefined = undefined;
  constructor(private fb: FormBuilder,
              private userInfoService: UserInfoService,
              private loginService: AccountService,
              private route: ActivatedRoute,
              private router: Router,
              private http: HttpClient,
              private cookieService: CookieService
              ) { }

  ngOnInit() {
    this.cookieService.deleteAll();
  }

  // login() {
  //   const input = this.profileForm.value;
  //   this.loginService.findOneUser(input.email, input.password).subscribe((resp) => {
  //     if (!resp) {
  //       this.isInvalid = true;
  //       console.log('no data find');
  //       return;
  //     }
  //     this.userInfo = resp;
  //     this.isInvalid = false;
  //     this.sendUserInfo(this.userInfo);
  //     this.router.navigate(['/main']);
  //   }, err => {
  //     this.isInvalid = true;
  //     console.warn(err);
  //   });
  // }

  login(f: NgForm) {
    this.http.post('http://globalportal.test/login',
      {
        email: f.controls['email'].value,
        password: f.controls['password'].value,
      })
      .subscribe(data => {
        if (data === true) {
          this.router.navigate(['/main']);
          this.cookieService.set('loginCheck', 'testCookieValue');
          this.cookieValue = this.cookieService.get('loginCheck');
        } else {
          this.isVisible = true;
        }
      });
  }

  sendUserInfo(msg: User): void {
    this.userInfoService.sendMessage(msg);
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }
}
