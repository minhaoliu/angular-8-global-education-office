import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorApplicationComponent } from './mentor-application.component';

describe('MentorApplicationComponent', () => {
  let component: MentorApplicationComponent;
  let fixture: ComponentFixture<MentorApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
