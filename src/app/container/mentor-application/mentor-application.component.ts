import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-mentor-application',
  templateUrl: './mentor-application.component.html',
  styleUrls: ['./mentor-application.component.less']
})
export class MentorApplicationComponent implements OnInit {
  private condition = false;
  selectedFile = null;
  isVisible = false;

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }



  onFileSelected(event) {
    console.log(event);
    this.selectedFile = event.target.files[0];
  }

  onUpload() {

  }

  constructor(private cookieService: CookieService) { }

  // const fileInput = document.querySelector('#file-js-example input[type=file]');
  // fileInput.onchange = () => {
  //   if (fileInput.files.length > 0) {
  //     const fileName = document.querySelector('#file-js-example .file-name');
  //     fileName.textContent = fileInput.files[0].name;
  //   }
  // }

  ngOnInit() {
    this.condition = this.cookieService.check('loginCheck');
  }

}
