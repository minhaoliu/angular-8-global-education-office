import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-display-mentor',
  templateUrl: './display-mentor.component.html',
  styleUrls: ['./display-mentor.component.less']
})
export class DisplayMentorComponent implements OnInit {
  list;
  private condition = false;
  private isVisible = false;

  constructor(private http: HttpClient, private jsonp: HttpClientJsonpModule, private cookieService: CookieService) { }

  getData() {
    this.http.get('http://globalportal.test/mentorsAndStudents').subscribe((data) => {
      console.log(data);
      this.list = data;
    });
  }

  delete(id){
    this.http.get('http://globalportal.test/event/delete/' + id).subscribe((data) => {
      console.log(data);
      this.isVisible = true;
    });
  }

  edit(id) {

  }

  handleOk(): void {
    this.isVisible = false;
    location.reload();
  }

  handleCancel(): void {
    this.isVisible = false;
    location.reload();
  }

  ngOnInit() {
    this.getData();
    this.condition = this.cookieService.check('loginCheck');
  }

}
