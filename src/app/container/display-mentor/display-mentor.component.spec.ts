import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayMentorComponent } from './display-mentor.component';

describe('DisplayMentorComponent', () => {
  let component: DisplayMentorComponent;
  let fixture: ComponentFixture<DisplayMentorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayMentorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayMentorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
