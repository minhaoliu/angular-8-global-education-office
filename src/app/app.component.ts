import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserInfoService } from '../app/service/user-info.service'
import { faUser } from '@fortawesome/free-solid-svg-icons';

declare var paypal;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'GlobalEducation';
  faUser = faUser;

  constructor(private router: Router,
              private userInfoService : UserInfoService){

  }
  editUser() {
    this.router.navigate(['/register']);
  }

  signOut() {
    // clear message
    this.userInfoService.clearMessage();
  }

}
