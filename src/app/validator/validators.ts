import { FormControl, FormGroup } from '@angular/forms';

export function mobileValidator(control: FormControl): any {
  var myreq = /^\d{10}$/;
  let valid = myreq.test(control.value);
  console.log("Validate result：" + valid);
  return valid ? null : {mobile : true};
}


export function equalValidator(group: FormGroup): any {
  let password: FormControl = group.get("password") as FormControl;
  let pconfirm: FormControl = group.get("pconfirm") as FormControl;
  let valid: boolean = password.value === pconfirm.value;
  console.log("password validate result：" + valid);
  return valid ? null : {equal : {desc:"Passwords does not match"}};
}

export function emailValidator(control: FormControl): any {
  var myreq = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  let valid = myreq.test(control.value);
  return valid ? null : {email : true};
}
