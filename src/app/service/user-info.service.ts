import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {User} from '../../utils';
/**
 * use to set and sent user info
 */
@Injectable({
  providedIn: 'root'
})
export class UserInfoService {
  private subject = new Subject<User>();

  user: User| undefined = undefined;

  sendMessage(userInfo: User) {
      this.subject.next(userInfo);
      this.user = userInfo;
  }

  clearMessage() {
      this.subject.next();
  }

  getMessage(): Observable<User> {
      return this.subject.asObservable();
  }
}
