import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { User, MockUser, UserStatus } from '../../utils';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  findOneUser(email: string, password: string): Observable<User> {
    // user mocked USER API
    const aUser = MockUser.find((user: User) => user.email === email && user.password === password);
    return of(aUser);
    //return this.http.get<User>(`/api/login/${email}/${password}`);
  }

  listAll(): Observable<User[]> {
    // return of(MockUser);
    return this.http.get<User[]>('/api/users');
  }

  searchUser(input: string): Observable<User[]> {
    if (input.length === 0 || input === ' ') {
      return of([]);
    }
    return this.http.get<User[]>(`/api/searchUser/${input.trim()}`);
  }
  findIndex(account:User){
    return account.id
  }

  updateAccount(account: User, preAccount: User | undefined) {
    /**
     * if preAccount exist, means update, set preAccount email as param
     * if not exist, means create, set account email as param
     */
    // if (preAccount !== undefined) {
    //   const id = this.findIndex(preAccount);
    //   if (parseInt(id)> -1) {
    //     MockUser.splice(parseInt(id), 1);
    //   } else {
    //     console.log('user account not exist');
    //   }
    // }
    // MockUser.push(account);
    const email = preAccount ? preAccount.email : account.email;
    return this.http.put<User>(`/api/editAccount/${email}`, account);
  }

  deleteAccount(account: User) {
    // const id = this.findIndex(account);
    // if (parseInt(id) > -1) {
    //   MockUser.splice(parseInt(id), 1);
    // }
    return this.http.delete<User>(`/api/delAccount/${account.email}`);
  }
}
