import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent} from './container/mainpage/mainpage.component';
import { LoginComponent } from './container/login/login.component';
import { AirportformComponent } from './container/airportform/airportform.component';
import { MentorApplicationComponent } from './container/mentor-application/mentor-application.component';
import { registerLocaleData } from '@angular/common';
import { RegisterComponent } from './container/register/register.component';
import { ViewStudentsComponent } from './container/view-students/view-students.component';
import { CreateEventComponent } from './container/create-event/create-event.component';
import { ViewEventComponent } from './container/view-event/view-event.component';
import { ViewpickupComponent } from './container/viewpickup/viewpickup.component';
import { KanbanComponent } from './container/kanban/kanban.component';
import { DisplayMentorComponent } from './container/display-mentor/display-mentor.component';


const routes: Routes = [
  {path: 'main', component: MainpageComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'viewstudents', component: ViewStudentsComponent},
  {path: 'viewevents', component: ViewEventComponent},
  {path: 'viewpickups', component: ViewpickupComponent},
  {path: 'createevent', component: CreateEventComponent},
  {path: 'login', component: LoginComponent},
  {path: 'airportform', component: AirportformComponent},
  {path: 'mentorapplication', component: MentorApplicationComponent},
  {path: 'display-mentor', component: DisplayMentorComponent},
  {path: 'kanban', component: KanbanComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
